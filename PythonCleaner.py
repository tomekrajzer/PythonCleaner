from os import listdir, stat, path, remove
from shutil import rmtree
from datetime import datetime, timedelta

DIRS = [
    '/Users/TOMEK/Downloads',
    '/Users/TOMEK/Desktop/to_remove'
]


def main():
    all_files = []

    for full_path in DIRS:
        all_files.extend([path.join(full_path, f) for f in listdir(full_path)])

    for file in all_files:
        info = stat(file)
        today_timestamp = round((datetime.now() - timedelta(7)).timestamp())
        if info.st_atime < today_timestamp:
            remove(file) if path.isfile(file) else rmtree(file)
            print("Removing file {}".format(file))


if __name__ == '__main__':
    main()
