# Python File Cleaner

Python script to clear unused files

## Getting Started

I always have a some directories where I collect a lot of temporary files and with time forgot to clear them.
So I make this simple script. 
### Prerequisites

If you want to use this script, just download this file, place wherever you want on yor computer/server and edit definition 
of DIRS variable.
```
DIRS = [
    '/Users/TOMEK/Downloads',
    '/Users/TOMEK/Desktop/to_remove'
]
```
In my case I using cron to run this file. If you want to create new cron rule, just
used command:

```
crontab -e
```

My role looks like that:

```
0 20 * * *	/usr/local/bin/python3 /Users/TOMEK/Repositories/PythonCleaner/PythonCleaner.py > /Users/TOMEK/Desktop/cron.log 2>&1
```
If you want to check that your rule was installed just use;
```
crontab -l
```
Give an example

## Built With

* Python 3

## Authors

* **Tomasz Rajzer** - *Initial work* - [tomekrajzer](https://gitlab.com/tomekrajzer)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


